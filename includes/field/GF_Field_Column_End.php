<?php

	class GF_Field_Column_End extends GF_Field {
		public $type = 'column_end';

        public function get_field_label( $force_frontend_label, $value ) {
            $field_label = esc_html__( 'Column End', 'gf-form-multicolumn' );

            return $field_label;
        }

        public function get_form_editor_inline_script_on_page_render() {
 
            // set the default field label for the field
            $script = sprintf( "function SetDefaultValues_%s(field) {field.label = '%s';}", $this->type, $this->get_form_editor_field_title() ) . PHP_EOL;
         
            return $script;
        }
        /*
         * Add button into the multicolumn field group
         */
        public function get_form_editor_button() {
            return array(
                'group' => GF_BSCOLUMN_FIELD_GROUP_TITLE,
                'text'  => $this->get_form_editor_field_title(),
            );
        }

        public function get_form_editor_field_title() {
            return esc_attr__( 'Column End', 'gfbscolumn' );
        }

        public function get_form_editor_field_settings() {
            return array(
                'css_class_setting',
            );
        }
	}
	GF_Fields::register( new GF_Field_Column_End() );