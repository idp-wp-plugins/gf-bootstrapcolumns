<?php
/*
 * Plugin Name: GravityForms: Bootstrap Columns
 * Plugin URI: http://imagedesign.pro
 * Description: Allows addition of responsive Boostrap 4 columns (and multiple rows of multiple columns) to Gravity Forms. Based on WebHolism's Multiple Columns for Gravity Forms plugin
 * Author: imageDESIGN
 * Author URI: http://imagedesign.pro
 * Version: 1.0.3
 * License: GPLv3
 * License URI: http://www.gnu.org/licenses/gpl-3.0
 GitLab Plugin URI: https://gitlab.com/idp-wp-plugins/gf-bootstrapcolumns
 */

    define( 'GF_BSCOLUMN_VERSION', '1.0.0' );
    define( 'GF_BSCOLUMN_TITLE', 'Boostrap Columns' );
    define( 'GF_BSCOLUMN_FIELD_GROUP_TITLE', 'Column Fields' );

	add_action( 'gform_loaded', [ 'GF_BSColumn_Bootstrap', 'load' ], 5 );

	class GF_BSColumn_Bootstrap {

		public static function load() {
			if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
				return;
			}
			require_once 'class-gf-bootstrap-columns.php';
			GFAddOn::register( 'GFBSColumn' );
		}
	}

	function gf_simple_addon() {
		return GFBSColumn::get_instance();
	}

add_filter( 'gform_entry_list_columns', 'set_columns', 10, 2 );
function set_columns( $table_columns, $form_id ){
	//echo '<pre>'.print_r($table_columns,1).'</pre>'; exit;
	foreach( $table_columns as $k => $tc ){
		//echo $tc.'<br>';
		if( $tc == 'Column Start' || $tc == 'Column Break' || $tc == 'Column End'){
			unset($table_columns[$k]);
		} 
	}
//	echo '<pre>'.print_r($table_columns,1).'</pre>'; exit;
	return $table_columns;
}